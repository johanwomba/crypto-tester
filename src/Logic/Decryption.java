package Logic;


import java.util.Base64;

public class Decryption {


    public String VigenereDecrypt(String message, String key) {

        String decrypted = "";
        message = message.toUpperCase();

        for (int i = 0, j = 0; i < message.length(); i++) {
            char c = message.charAt(i);
            if (c < 'A' || c > 'Z') continue;
            decrypted += (char)((c - key.charAt(j) + 26) % 26 + 'A');
            j = ++j % key.length();
        }
        return decrypted;
    }


    public String fromBinary (String message) {

        message = message.replaceAll("\\s","");
        String originalMessage = "";

        for (int i = 0; i <= message.length() - 8; i+=8) {

            int k = Integer.parseInt(message.substring(i, i+8), 2);
            originalMessage += (char) k;
        }
        return originalMessage;
    }

    public String fromHex(String hex) {

        StringBuilder output = new StringBuilder();
        for (int i = 0; i < hex.length(); i+=2) {
            String str = hex.substring(i, i+2);
            output.append((char)Integer.parseInt(str, 16));
        }
        return output.toString();
    }

    public String fromBase64 (String message) {

        byte[] decrypted = Base64.getDecoder().decode(message.getBytes());
        return new String(decrypted);
    }
}
