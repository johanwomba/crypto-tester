package Logic;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.*;
import java.util.Base64;

public class Encryption {

    public String toCeasar (String message) {

        String encrypted = "";

        for (int i = 0; i < message.length(); i++) {
            char c = message.charAt(i);
            if       (c >= 'a' && c <= 'm') c += 13;
            else if  (c >= 'A' && c <= 'M') c += 13;
            else if  (c >= 'n' && c <= 'z') c -= 13;
            else if  (c >= 'N' && c <= 'Z') c -= 13;

            encrypted += c;
        }
        return encrypted;
    }

    public String toVigenere(String message, String key) {

        String encrypted = "";
        message = message.toUpperCase();

        for (int i = 0, j = 0; i < message.length(); i++) {

            char c = message.charAt(i);
            if (c < 'A' || c > 'Z') continue;
            encrypted += (char)((c + key.charAt(j) - 2 * 'A') % 26 + 'A');
            j = ++j % key.length();
        }
        return encrypted;
    }

    public String toHASH (String message, String method) {

        try {
            MessageDigest md = MessageDigest.getInstance(method);
            md.reset();
            md.update(message.getBytes("UTF-8"));
            byte[] resultByte = md.digest();
            message = String.format("%01x", new java.math.BigInteger(1, resultByte));

        } catch (NoSuchAlgorithmException e) {
            System.err.println("Couldn't find hash-algorithm.");
        } catch (UnsupportedEncodingException ex) {
            System.err.println("Couldn't hash the complete message.");
        }
        return message;
    }

    public String toAES(String message) {

        byte [] toEncrypt = message.getBytes();
        String encrypted = "";

        try {
            KeyGenerator AESgen = KeyGenerator.getInstance("AES");
            AESgen.init(128);
            SecretKeySpec AESkey = (SecretKeySpec)AESgen.generateKey();

            Cipher AEScipher = Cipher.getInstance("AES");
            AEScipher.init(Cipher.ENCRYPT_MODE, AESkey);

            byte [] cipherData = AEScipher.doFinal(toEncrypt);
            encrypted = new String(cipherData);

        } catch (InvalidKeyException | IllegalBlockSizeException | NoSuchAlgorithmException e) {
            System.out.println("It broke...");
        }  catch (BadPaddingException | NoSuchPaddingException e) {
            System.out.println("It broke...");
        }
        return encrypted;
    }

    public String toRSA (String message) {

        byte[] toEncrypt = message.getBytes();
        byte[] cipherData;
        String encrypted = "";

        try {

            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(1024);
            KeyPair kp = kpg.genKeyPair();
            Key publicKey = kp.getPublic();
            Key privateKey = kp.getPrivate();

            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            cipherData = cipher.doFinal(toEncrypt);
            encrypted = new String(cipherData);

        } catch (InvalidKeyException | IllegalBlockSizeException | NoSuchAlgorithmException e) {
            System.out.println("It broke...");
        }  catch (BadPaddingException | NoSuchPaddingException e) {
            System.out.println("It broke...");
        }
        return encrypted;
    }

    public String toDES(String message) {

        byte [] toEncrypt = message.getBytes();
        String encrypted = "";
        try {

            KeyGenerator keygen = KeyGenerator.getInstance("DES");
            SecretKey key = keygen.generateKey();
            Cipher desCipher;
            desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            desCipher.init(Cipher.ENCRYPT_MODE, key);

            byte [] encrypt = desCipher.doFinal(toEncrypt);
            encrypted = new String(encrypt);

        } catch (InvalidKeyException | IllegalBlockSizeException | NoSuchAlgorithmException e) {
            System.out.println("It broke...");
        }  catch (BadPaddingException | NoSuchPaddingException e) {
            System.out.println("It broke...");
        }
        return encrypted;
    }
    public String toBinary(String message) {

        byte[] bytes = message.getBytes();
        StringBuilder binary = new StringBuilder();

        for (byte b : bytes) {

            int val = b;

            for (int i = 0; i < 8; i++) {

                binary.append((val & 128) == 0 ? 0 : 1);
                val <<= 1;
            }
            binary.append(' ');
        }
        return binary.toString();
    }

    public String toHex(String message) {

        return String.format("%01x", new BigInteger(1, message.getBytes()));
    }

    public String toBase64 (String message) {

        String encrypted = Base64.getEncoder().encodeToString(message.getBytes());
        return encrypted;
    }
}
