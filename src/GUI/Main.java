package GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("mainScene.fxml"));
        primaryStage.setTitle("Crypto Tester");
        primaryStage.getIcons().add(new Image("http://worldartsme.com/images/locked-button-clipart-1.jpg"));
        root.getStylesheets().add(getClass().getResource("style.css").toExternalForm());  //gets style from CSS
        primaryStage.setScene(new Scene(root, 790, 565));
        primaryStage.show();
        primaryStage.setResizable(false);
    }


    public static void main(String[] args) {
        launch(args);
    }
}
