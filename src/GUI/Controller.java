package GUI;

import Logic.Encryption;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;

public class Controller {

    @FXML
    private TextField cleartextField;
    @FXML
    private TextField keyField;
    @FXML
    private TextArea ciphertextArea;
    @FXML
    private ComboBox whatMethod;
    @FXML
    private Label infoBox;
    @FXML
    private CheckMenuItem decryptMode;

    Encryption encrypt = new Encryption();

    ObservableList<String> options =
            FXCollections.observableArrayList(
                    "Ceasar Cipher", "Vigen\u00E8re Cipher",
                    "RSA", "AES", "DES", "MD5", "SHA-1",
                     "SHA-512", "Binary", "Hexadecimal", "Base64"
            );

    private String message = "";
    private String key = "";

    @FXML
    private void showMessage(final String text) {
        ciphertextArea.clear();
        Platform.runLater(() -> ciphertextArea.appendText(text));
    }
    @FXML
    private void showInfo(final String text) {
        infoBox.setText("");
        Platform.runLater(() -> infoBox.setText(text));
    }
    @FXML
    private void sendMessage() {

        if (!cleartextField.equals("")) {
            message = cleartextField.getText();
            key = keyField.getText();

            if (whatMethod.getValue().equals("Ceasar Cipher")) {
                showMessage(encrypt.toCeasar(message));
                showInfo("The Ceasar Cipher is a mono-alphabetic cipher developed by Julius Ceasar around 500 BC. " +
                        "This classic encryption method shifts all letters thirteen steps forward in the alphabet.");
            }
            else if (whatMethod.getValue().equals("Vigen\u00E8re Cipher")) {
                if (key.equals("")) {
                    showInfo("You need to enter a keyword.");
                }
                else {
                    showMessage(encrypt.toVigenere(message, key));
                    showInfo("Blaise de Vigen\u00E8re developed this poly-alphabetic substitution cipher somewhere around " +
                            "the 16th century. Like the Ceasar Cipher this encryption method shifts the letters around " +
                            "the alphabet, but here the alphabet is the keyword.");
                }
            }
            else if (whatMethod.getValue().equals("RSA")) {
                showMessage(encrypt.toRSA(message));
                showInfo("RSA was developed in 1977 by Ron Rivest, Adi Shamir and Len Adleman. It is a public key encryption" +
                        " method which is widely used in hardware and operating systems. RSA is used in SSL-transactions, " +
                        "key exchanges and data encryption/decryption.");
            }
            else if (whatMethod.getValue().equals("AES")) {
                showMessage(encrypt.toAES(message));
                showInfo("AES (Advanced Encryption Standard), originally called Rijndael, was created by Vincent Rijmen and" +
                        " John Daemen. NIST declared AES to be the new encryption standard in 2001. The key length in AES " +
                        "can be 128, 192 or 256 bits depending on implementation and country. NSA consider AES-256 to be " +
                        "secure enough to encrypt top secret documents.");
            }
            else if (whatMethod.getValue().equals("DES")) {
                showMessage(encrypt.toDES(message));
                showInfo("DES (Data Encryption Standard) was created by Horst Fiestel (among others) in 1974, originally " +
                        "called Lucifer. DES was elected the new encryption standard in 1977. This block cipher has a 56" +
                        " bit key and is today considered obsolete. The publication of DES prompted a lot of research in cryptography.");
            }
            else if (whatMethod.getValue().equals("MD5")) {
                showMessage(encrypt.toHASH(message, "MD5"));
                showInfo("Created by Ron Rivest based on the cryptographic structure developed by Ralph Merkle and Ivan Damg\u00E5rd." +
                        " However, cryptoanalysts found severe weaknesses in MD5, so it is considered obsolete. ");
            }
            else if (whatMethod.getValue().equals("SHA-1")) {
                showMessage(encrypt.toHASH(message, "SHA-1"));
                showInfo("SHA-1 (Secure hash algorithm) was created by NSA in 1993. It produces a 160 bit hash. This hash function has " +
                        "since 2010 been considered obsolete.");
            }
            else if (whatMethod.getValue().equals("SHA-512")) {
                showMessage(encrypt.toHASH(message, "SHA-512"));
                showInfo("After SHA-1, the NSA created the SHA-2 family, which contains SHA-256, SHA-384 and SHA-512. This " +
                        "family of algorithms is based on Merkle and Damg\u00E5rds original hash-design.");
            }
            else if (whatMethod.getValue().equals("Binary")) {
                showMessage(encrypt.toBinary(message));
                showInfo("Binary encoding transforms every character into 8-bit binary data. This is how the computer interprets" +
                        " all data.");
            }
            else if (whatMethod.getValue().equals("Hexadecimal")) {
                showMessage(encrypt.toHex(message));
                showInfo("Hexadecimal, also known as Base 16, uses sixteen distinct symbols. The numbers 0-9 and the characters " +
                        "A-F (or alternatively a-f.) This encoding method is widely used by designers and programmers.");
            }
            else if (whatMethod.getValue().equals("Base64")) {
                showMessage(encrypt.toBase64(message));
                showInfo("This encoding method is used for encoding binary data to 7-bits ASCII for distribution through" +
                        " e-mail, amongst others.");
            }
            else if (whatMethod.getValue().equals("")) {
                showMessage(message);
                showInfo("");
            }
            else {
                showMessage(message);
                showInfo("");
            }

        }
        message = "";
        key = "";

    }

    @FXML
    private void initialize() {

        whatMethod.setValue("Encryption Method");
        whatMethod.setItems(options);
    }
}
